package main

import (
	"fmt"
	"isi-test/delivery"
	"isi-test/delivery/container"
)

func main() {
	Container := container.SetupContainer()
	serverHttp := delivery.ServerHttp(Container)
	serverHttp.Listen(fmt.Sprintf(":%d", Container.ConfigENV.Port))
}
