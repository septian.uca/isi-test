package entity

type Customers struct {
	NIK           string `db:"nik" json:"nik"`
	Name          string `db:"name" json:"name"`
	PhoneNumber   string `db:"phone_number" json:"phone_number"`
	AccountNumber string `db:"account_number" json:"account_number"`
	CreatedAt     string `db:"created_at" json:"created_at"`
}

func (c Customers) TableName() string {
	return "customers"
}

func (c Customers) FirstRow(customers []Customers) *Customers {
	if len(customers) == 0 {
		return nil
	}

	return &customers[0]
}
