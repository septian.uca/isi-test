package customer

import (
	"isi-test/domain/customer/dto"
	"isi-test/domain/customer/service"
	"isi-test/domain/shared/response"
	"isi-test/domain/shared/validator"

	"github.com/gofiber/fiber/v2"
)

type CustomerHandler interface {
	RegistatioHandler(c *fiber.Ctx) error
}

type customerHandler struct {
	customerService service.CustomerService
}

func NewCustomerHandler(custService service.CustomerService) CustomerHandler {
	return &customerHandler{
		customerService: custService,
	}
}

func (ch customerHandler) RegistatioHandler(c *fiber.Ctx) error {
	ctx := c.Context()
	bodyRequest := new(dto.RegistrationRequest)
	if err := c.BodyParser(bodyRequest); err != nil {
		return response.ResponseErrors(c, err)
	}

	errResp := validator.ValidateStruct(bodyRequest)
	if errResp != nil {
		return response.ResponseValidationError(c, "validation error", errResp)
	}

	data, err := ch.customerService.InsertCustomerService(ctx, *bodyRequest)
	if err != nil {
		return response.ResponseErrors(c, err)
	}

	return response.ResponseOK(c, "registration new customers successfully", data)
}
