package service

import (
	"context"
	"fmt"
	"isi-test/domain/customer/dto"
	"isi-test/domain/customer/entity"
	"isi-test/domain/customer/repository"
	sharedConstant "isi-test/domain/shared/constant"
	errorHandler "isi-test/infrastructure/error"
	"math/rand"
	"time"
)

type CustomerService interface {
	InsertCustomerService(ctx context.Context, payload dto.RegistrationRequest) (dto.RegistrationResponse, error)
	FindCustomerByAccountNumberService(ctx context.Context, accountNumber string) (*entity.Customers, error)
}

type customerService struct {
	customerRepo repository.CustomerRepository
}

func NewCustomerService(customerRepo repository.CustomerRepository) CustomerService {
	return &customerService{
		customerRepo: customerRepo,
	}
}

func (cs customerService) InsertCustomerService(ctx context.Context, payload dto.RegistrationRequest) (response dto.RegistrationResponse, err error) {

	customerDetail, err := cs.customerRepo.FindCustomerRepository(ctx, payload.NIK, payload.PhoneNumber)
	if err != nil {
		return
	}

	if customerDetail != nil {
		err = errorHandler.New(sharedConstant.ErrBadRequest, fmt.Errorf("nik or phone number has been used"))
		return
	}

	insertData := entity.Customers{
		NIK:           payload.NIK,
		Name:          payload.Name,
		PhoneNumber:   payload.PhoneNumber,
		AccountNumber: cs.generatAccountNumber(),
	}

	err = cs.customerRepo.InsertCustomerRepository(ctx, insertData)
	if err != nil {
		return
	}

	response = dto.RegistrationResponse{
		NIK:           insertData.NIK,
		Name:          insertData.Name,
		PhoneNumber:   insertData.PhoneNumber,
		AccountNumber: insertData.AccountNumber,
	}
	return
}

func (cs customerService) FindCustomerByAccountNumberService(ctx context.Context, accountNumber string) (*entity.Customers, error) {
	return cs.customerRepo.FindCustomerByAccountNumberRepository(ctx, accountNumber)
}

func (cs customerService) generatAccountNumber() (accountNumber string) {
	const charset = "123456789"

	rand.Seed(time.Now().UnixNano())

	result := make([]byte, 15)
	result[0] = '1'

	for i := 1; i < 15; i++ {
		result[i] = charset[rand.Intn(len(charset))]
	}

	return string(result)
}
