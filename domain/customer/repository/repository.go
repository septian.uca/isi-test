package repository

import (
	"context"
	"fmt"
	"isi-test/domain/customer/entity"
	sharedConstant "isi-test/domain/shared/constant"
	"isi-test/infrastructure/database"
	errorHandler "isi-test/infrastructure/error"
)

type CustomerRepository interface {
	InsertCustomerRepository(ctx context.Context, insertData entity.Customers) error
	FindCustomerRepository(ctx context.Context, nik, phoneNumber string) (*entity.Customers, error)
	FindCustomerByAccountNumberRepository(ctx context.Context, accountNumber string) (*entity.Customers, error)
}

type customerRepo struct {
	db *database.Database
}

func NewCustomerRepository(db *database.Database) CustomerRepository {
	return &customerRepo{
		db: db,
	}
}

func (c customerRepo) InsertCustomerRepository(ctx context.Context, insertData entity.Customers) (err error) {
	customer := entity.Customers{}
	query := fmt.Sprintf(`INSERT INTO %s (
		nik,
		name,
		phone_number,
		account_number)
		VALUES 
		($1, $2, $3, $4)
	`, customer.TableName())

	tx, err := c.db.BeginTxx(ctx, nil)

	if err != nil {
		err = errorHandler.New(sharedConstant.ErrDatabase, err)
		return
	}

	_, err = tx.ExecContext(ctx, query,
		insertData.NIK,
		insertData.Name,
		insertData.PhoneNumber,
		insertData.AccountNumber)

	if err != nil {
		tx.Rollback()
		err = errorHandler.New(sharedConstant.ErrDatabase, err)
		return
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		err = errorHandler.New(sharedConstant.ErrDatabase, err)
		return
	}

	return
}

func (c customerRepo) FindCustomerRepository(ctx context.Context, nik, phoneNumber string) (customerData *entity.Customers, err error) {
	customer := entity.Customers{}
	customers := &[]entity.Customers{}

	query := fmt.Sprintf(`SELECT * FROM %s WHERE phone_number = $1 OR nik = $2`, customer.TableName())

	err = c.db.SelectContext(ctx, customers, query, phoneNumber, nik)
	if err != nil {
		err = errorHandler.New(sharedConstant.ErrDatabase, err)
		return
	}

	customerData = customer.FirstRow(*customers)

	return
}

func (c customerRepo) FindCustomerByAccountNumberRepository(ctx context.Context, accountNumber string) (customerData *entity.Customers, err error) {
	customer := entity.Customers{}
	customers := &[]entity.Customers{}

	query := fmt.Sprintf(`SELECT * FROM %s WHERE account_number = $1 `, customer.TableName())

	err = c.db.SelectContext(ctx, customers, query, accountNumber)
	if err != nil {
		err = errorHandler.New(sharedConstant.ErrDatabase, err)
		return
	}

	customerData = customer.FirstRow(*customers)

	return
}
