package dto

type RegistrationResponse struct {
	NIK           string `json:"nik"`
	Name          string `json:"nama"`
	PhoneNumber   string `json:"no_hp"`
	AccountNumber string `json:"no_rekening"`
}
