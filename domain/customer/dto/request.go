package dto

type RegistrationRequest struct {
	NIK         string `json:"nik"`
	Name        string `json:"nama"`
	PhoneNumber string `json:"no_hp"`
}
