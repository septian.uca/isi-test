package validator

import (
	"github.com/go-playground/validator/v10"
)

var validate = validator.New()

func ValidateStruct(input interface{}) (errResponse []*string) {
	err := validate.Struct(input)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element string
			element = err.StructNamespace() + " | " + err.Tag() + " | " + err.Param()
			errResponse = append(errResponse, &element)
		}
	}
	return errResponse
}
