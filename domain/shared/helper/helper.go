package helper

import (
	"strings"
	"time"
)

func GetCurrentTimeDB() string {
	p := GetUTCTime()

	return p.Format("15:04:05")
}

func GetTodayDateDB() string {
	p := GetUTCTime()
	return p.Format("2006-01-02")
}

func GetUTCTime() (t time.Time) {
	t = time.Now().UTC()

	return
}

func FormatDateFromDB(date string) string {
	ndate := strings.Split(date, "T")
	return ndate[0]
}

func FormatTimeFromDB(date string) string {
	ndate := strings.Split(date, "T")
	return ndate[1]
}
