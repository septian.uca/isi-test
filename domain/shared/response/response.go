package response

import (
	"fmt"
	"isi-test/domain/shared/constant"
	errorHandler "isi-test/infrastructure/error"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

type Response struct {
	Status  string      `json:"status"`
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data"`
}

type ErrorValidation struct {
	Status  string    `json:"status"`
	Message string    `json:"message,omitempty"`
	Remark  []*string `json:"remark"`
}

type ErrorMessage struct {
	Message string `json:"message,omitempty"`
}

func ResponseOK(c *fiber.Ctx, msg string, data interface{}) error {

	var (
		code = http.StatusOK
	)

	response := Response{
		Status:  constant.RES_SUCCESS,
		Message: msg,
		Data:    data,
	}

	return c.Status(code).JSON(response)
}

func ResponseValidationError(c *fiber.Ctx, msg string, remarks []*string) error {

	var (
		code = http.StatusBadRequest
	)

	response := ErrorValidation{
		Status:  constant.RES_ERR_VALIDATION,
		Message: msg,
		Remark:  remarks,
	}

	return c.Status(code).JSON(response)
}

func ResponseErrors(c *fiber.Ctx, err error) error {
	var (
		msg       string
		errorType string
	)
	var code int

	errorType, err = errorHandler.TrimMesssage(err)

	if errorType == constant.ErrBadRequest {
		remark := err.Error()
		remarks := []*string{
			&remark,
		}

		response := ErrorValidation{
			Status:  constant.RES_ERR_VALIDATION,
			Message: "error validation",
			Remark:  remarks,
		}

		return c.Status(code).JSON(response)
	} else {
		switch errorType {
		case constant.ErrAuth:
			msg = fmt.Sprintf("unauthorized : %s", err.Error())
			code = http.StatusUnauthorized

		case constant.ErrDatabase:
			msg = fmt.Sprintf("error database, internal server error : %s", err.Error())
			code = http.StatusInternalServerError

		case constant.ErrNotFound:
			msg = fmt.Sprintf("error data not found : %s", err.Error())
			code = http.StatusNotFound

		case constant.ErrUnProcessEntity:
			msg = fmt.Sprintf("error can not process data request : %s", err.Error())
			code = http.StatusUnprocessableEntity

		default:
			msg = fmt.Sprintf("something went wrong, please try again later : %s", err.Error())
			code = http.StatusServiceUnavailable
		}

		response := Response{
			Status:  constant.RES_ERR,
			Message: msg,
		}
		return c.Status(code).JSON(response)
	}

}
