package constant

const ( // INFO: status response
	RES_SUCCESS        = "SUCCESS"
	RES_ERR            = "ERROR"
	RES_ERR_VALIDATION = "ERROR VALIDATION"
)

const (
	FIBER_CONTEXT = "fiberCtx"
)
