package constant

const ( // INFO: error type
	ErrGeneral         = "error general"
	ErrDatabase        = "error database"
	ErrAuth            = "error authentication"
	ErrNotFound        = "error not found"
	ErrBadRequest      = "error bad request"
	ErrUnProcessEntity = "unprocessable entity"
)
