package repository

import (
	"context"
	"fmt"
	sharedConstant "isi-test/domain/shared/constant"
	localConstant "isi-test/domain/transaction/constant"
	"isi-test/domain/transaction/entity"
	"isi-test/infrastructure/database"
	errorHandler "isi-test/infrastructure/error"
)

type TransactionRepository interface {
	InsertTransactionRepository(ctx context.Context, insertData entity.Transactions) error
	GetListTransactionByAccountNumberRepository(ctx context.Context, accountNumber string) ([]entity.Transactions, error)
	GetBalanceByAccountNumberRepository(ctx context.Context, accountNumber string) (float64, error)
}

type transactionRepo struct {
	db *database.Database
}

func NewTransactionRepository(db *database.Database) TransactionRepository {
	return &transactionRepo{
		db: db,
	}
}

func (tr transactionRepo) InsertTransactionRepository(ctx context.Context, insertData entity.Transactions) (err error) {

	transaction := entity.Transactions{}
	query := fmt.Sprintf(`INSERT INTO %s (
		account_number,
		transaction_code,
		transaction_date,
		transaction_time,
		amount)
		VALUES 
		($1, $2, $3, $4, $5)
	`, transaction.TableName())

	tx, err := tr.db.BeginTxx(ctx, nil)

	if err != nil {
		err = errorHandler.New(sharedConstant.ErrDatabase, err)
		return
	}

	_, err = tx.ExecContext(ctx, query,
		insertData.AccountNumber,
		insertData.TransactionCode,
		insertData.TransactionDate,
		insertData.TransactionTime,
		insertData.Amount)

	if err != nil {
		tx.Rollback()
		err = errorHandler.New(sharedConstant.ErrDatabase, err)
		return
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		err = errorHandler.New(sharedConstant.ErrDatabase, err)
		return
	}

	return
}

func (tr transactionRepo) GetListTransactionByAccountNumberRepository(ctx context.Context, accountNumber string) (listTrans []entity.Transactions, err error) {
	transaction := entity.Transactions{}
	transactions := &[]entity.Transactions{}

	query := fmt.Sprintf(`SELECT * FROM %s WHERE account_number = $1`, transaction.TableName())
	err = tr.db.SelectContext(ctx, transactions, query, accountNumber)
	if err != nil {
		err = errorHandler.New(sharedConstant.ErrDatabase, err)
		return
	}

	listTrans = *transactions

	return
}

func (tr transactionRepo) GetBalanceByAccountNumberRepository(ctx context.Context, accountNumber string) (balance float64, err error) {

	query := fmt.Sprintf(`SELECT SUM(CASE WHEN transaction_code = '%s' THEN -amount ELSE amount END) AS balance FROM transactions WHERE account_number = $1`, localConstant.DEBIT)

	rows, err := tr.db.QueryContext(ctx, query, accountNumber)
	if err != nil {
		err = errorHandler.New(sharedConstant.ErrDatabase, err)
		return
	}
	defer rows.Close()

	if rows.Next() {
		for rows.Next() {
			errScan := rows.Scan(&balance)
			if errScan != nil {
				err = errorHandler.New(sharedConstant.ErrDatabase, errScan)
				return
			}
		}
	}

	return
}
