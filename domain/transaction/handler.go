package transaction

import (
	"isi-test/domain/transaction/service"

	"isi-test/domain/shared/response"
	"isi-test/domain/shared/validator"
	"isi-test/domain/transaction/dto"

	"github.com/gofiber/fiber/v2"
)

type TransactionHandler interface {
	WithdrawHandler(c *fiber.Ctx) error
	DepositHandler(c *fiber.Ctx) error
	BalanceHandler(c *fiber.Ctx) error
	MutationListHandler(c *fiber.Ctx) error
}

type transactionHandler struct {
	transactionService service.TransactionService
}

func NewTransactionHandler(transService service.TransactionService) TransactionHandler {
	return &transactionHandler{
		transactionService: transService,
	}
}

func (th transactionHandler) WithdrawHandler(c *fiber.Ctx) error {
	ctx := c.Context()
	bodyRequest := new(dto.TransactionRequest)
	if err := c.BodyParser(bodyRequest); err != nil {
		return response.ResponseErrors(c, err)
	}

	errResp := validator.ValidateStruct(bodyRequest)
	if errResp != nil {
		return response.ResponseValidationError(c, "validation error", errResp)
	}

	err := th.transactionService.WithdrawService(ctx, *bodyRequest)
	if err != nil {
		return response.ResponseErrors(c, err)
	}

	return response.ResponseOK(c, "withdraw successfully", nil)
}

func (th transactionHandler) DepositHandler(c *fiber.Ctx) error {
	ctx := c.Context()
	bodyRequest := new(dto.TransactionRequest)
	if err := c.BodyParser(bodyRequest); err != nil {
		return response.ResponseErrors(c, err)
	}

	errResp := validator.ValidateStruct(bodyRequest)
	if errResp != nil {
		return response.ResponseValidationError(c, "validation error", errResp)
	}

	err := th.transactionService.DepositService(ctx, *bodyRequest)
	if err != nil {
		return response.ResponseErrors(c, err)
	}

	return response.ResponseOK(c, "deposit successfully", nil)
}

func (th transactionHandler) BalanceHandler(c *fiber.Ctx) error {
	ctx := c.Context()
	accountNumber := c.Params("no_rekening")
	balance, err := th.transactionService.GetBalanceByAccountNumberService(ctx, accountNumber)
	if err != nil {
		return response.ResponseErrors(c, err)
	}

	return response.ResponseOK(c, "get balance successfully", balance)

}

func (th transactionHandler) MutationListHandler(c *fiber.Ctx) error {
	ctx := c.Context()
	accountNumber := c.Params("no_rekening")
	mutation, err := th.transactionService.GetListTransactionByAccountNumberService(ctx, accountNumber)
	if err != nil {
		return response.ResponseErrors(c, err)
	}

	return response.ResponseOK(c, "get mutation list successfully", mutation)
}
