package service

import (
	"context"
	"fmt"
	"isi-test/domain/customer/service"
	sharedConstant "isi-test/domain/shared/constant"
	sharedHelper "isi-test/domain/shared/helper"
	localConstant "isi-test/domain/transaction/constant"
	"isi-test/domain/transaction/dto"
	"isi-test/domain/transaction/entity"
	"isi-test/domain/transaction/repository"
	errorHandler "isi-test/infrastructure/error"
)

type TransactionService interface {
	WithdrawService(ctx context.Context, payload dto.TransactionRequest) error
	DepositService(ctx context.Context, payload dto.TransactionRequest) error
	GetListTransactionByAccountNumberService(ctx context.Context, accountNumber string) ([]dto.MutationRecordResponse, error)
	GetBalanceByAccountNumberService(ctx context.Context, accountNumber string) (dto.BalanceResponse, error)
}

type transactionService struct {
	transRepo       repository.TransactionRepository
	customerService service.CustomerService
}

func NewTransactionService(transRepo repository.TransactionRepository, customerService service.CustomerService) TransactionService {
	return &transactionService{
		transRepo:       transRepo,
		customerService: customerService,
	}
}

func (ts transactionService) WithdrawService(ctx context.Context, payload dto.TransactionRequest) (err error) {
	dataCustomer, err := ts.customerService.FindCustomerByAccountNumberService(ctx, payload.AccountNumber)
	if err != nil {
		return
	}

	if dataCustomer == nil {
		err = errorHandler.New(sharedConstant.ErrBadRequest, fmt.Errorf("account number is invalid"))
		return
	}

	if payload.Amount <= 0 {
		err = errorHandler.New(sharedConstant.ErrBadRequest, fmt.Errorf("nominal must be greater than 0"))
		return
	}

	balance, err := ts.GetBalanceByAccountNumberService(ctx, payload.AccountNumber)
	if err != nil {
		return
	}

	if balance.Balance < payload.Amount {
		msg := "unable to withdraw funds, your balance is less than nominal withdrawal"
		err = errorHandler.New(sharedConstant.ErrBadRequest, fmt.Errorf(msg))
	}

	insertData := entity.Transactions{
		AccountNumber:   payload.AccountNumber,
		TransactionCode: localConstant.DEBIT,
		TransactionDate: sharedHelper.GetTodayDateDB(),
		TransactionTime: sharedHelper.GetCurrentTimeDB(),
		Amount:          payload.Amount,
	}

	return ts.transRepo.InsertTransactionRepository(ctx, insertData)
}

func (ts transactionService) DepositService(ctx context.Context, payload dto.TransactionRequest) (err error) {
	dataCustomer, err := ts.customerService.FindCustomerByAccountNumberService(ctx, payload.AccountNumber)
	if err != nil {
		return
	}

	if dataCustomer == nil {
		err = errorHandler.New(sharedConstant.ErrBadRequest, fmt.Errorf("account number is invalid"))
		return
	}

	if payload.Amount <= 0 {
		err = errorHandler.New(sharedConstant.ErrBadRequest, fmt.Errorf("nominal must be greater than 0"))
		return
	}

	insertData := entity.Transactions{
		AccountNumber:   payload.AccountNumber,
		TransactionCode: localConstant.CREDIT,
		TransactionDate: sharedHelper.GetTodayDateDB(),
		TransactionTime: sharedHelper.GetCurrentTimeDB(),
		Amount:          payload.Amount,
	}

	return ts.transRepo.InsertTransactionRepository(ctx, insertData)
}

func (ts transactionService) GetListTransactionByAccountNumberService(ctx context.Context, accountNumber string) (result []dto.MutationRecordResponse, err error) {
	dataCustomer, err := ts.customerService.FindCustomerByAccountNumberService(ctx, accountNumber)
	if err != nil {
		return
	}

	if dataCustomer == nil {
		err = errorHandler.New(sharedConstant.ErrBadRequest, fmt.Errorf("account number is invalid"))
		return
	}

	dbResult, err := ts.transRepo.GetListTransactionByAccountNumberRepository(ctx, accountNumber)
	if err != nil {
		return
	}

	for _, i := range dbResult {
		result = append(result, dto.MutationRecordResponse{
			DateTime:        sharedHelper.FormatDateFromDB(i.TransactionDate) + " " + sharedHelper.FormatTimeFromDB(i.TransactionTime),
			TransactionCode: i.TransactionCode,
			Amount:          i.Amount,
		})
	}

	return
}

func (ts transactionService) GetBalanceByAccountNumberService(ctx context.Context, accountNumber string) (result dto.BalanceResponse, err error) {
	dataCustomer, err := ts.customerService.FindCustomerByAccountNumberService(ctx, accountNumber)
	if err != nil {
		return
	}

	if dataCustomer == nil {
		err = errorHandler.New(sharedConstant.ErrBadRequest, fmt.Errorf("account number is invalid"))
		return
	}

	balance, err := ts.transRepo.GetBalanceByAccountNumberRepository(ctx, accountNumber)
	if err != nil {
		return
	}

	result.Balance = balance
	return
}
