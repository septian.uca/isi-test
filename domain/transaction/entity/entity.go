package entity

type Transactions struct {
	ID              int     `db:"id" json:"id"`
	AccountNumber   string  `db:"account_number" json:"account_number"`
	TransactionCode string  `db:"transaction_code" json:"transaction_code"`
	TransactionDate string  `db:"transaction_date" json:"transaction_date"`
	TransactionTime string  `db:"transaction_time" json:"transaction_time"`
	Amount          float64 `db:"amount" json:"amount"`
	CreatedAt       string  `db:"created_at" json:"created_at"`
}

func (t Transactions) TableName() string {
	return "transactions"
}
