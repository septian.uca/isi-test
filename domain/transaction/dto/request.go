package dto

type TransactionRequest struct {
	AccountNumber string  `json:"no_rekening"`
	Amount        float64 `json:"nominal"`
}
