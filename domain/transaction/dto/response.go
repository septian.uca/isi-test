package dto

type BalanceResponse struct {
	Balance float64 `json:"saldo"`
}

type MutationRecordResponse struct {
	DateTime        string  `json:"waktu"`
	TransactionCode string  `json:"kode_transaksi"`
	Amount          float64 `json:"nominal"`
}
