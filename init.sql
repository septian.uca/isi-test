
CREATE TABLE public.customers (
	nik varchar(16) NOT NULL,
	"name" varchar(125) NOT NULL,
	phone_number varchar(14) NOT NULL,
	account_number varchar(15) NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT customers_pkey PRIMARY KEY (nik, phone_number, account_number)
);

CREATE TABLE public."transaction" (
	id SERIAL NOT null primary KEY,
	account_number varchar(15) NOT NULL,
	transaction_code char(1) NOT NULL,
	transaction_date date NOT NULL,
	transaction_time time NOT NULL,
	amount decimal(15, 2) NOT NULL,
	created_at timestamp NOT null DEFAULT NOW()
);
