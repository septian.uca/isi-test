package error

import (
	"errors"
	"fmt"
	"strings"
)

func New(errType string, err error) error {
	return fmt.Errorf("%s | %w", errType, err)
}

func TrimMesssage(err error) (errType string, newErr error) {
	errs := strings.Split(err.Error(), "|")

	errType = strings.TrimSpace(errs[0])
	newErr = errors.New(strings.TrimSpace(errs[1]))

	return
}
