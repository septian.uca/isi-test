package constant

const (
	ErrConnectDB = "error when try connect to database : %w"
	ErrLoadENV   = "error when load env : %w"
)

const (
	ErrConvertStringToInt = "error when convert string to int : %w"
)
