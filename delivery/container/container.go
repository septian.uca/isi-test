package container

import (
	"fmt"
	config "isi-test/config"
	"isi-test/domain/customer"
	customer_repository "isi-test/domain/customer/repository"
	customer_service "isi-test/domain/customer/service"
	"isi-test/domain/transaction"
	trans_repository "isi-test/domain/transaction/repository"
	trans_service "isi-test/domain/transaction/service"
	database "isi-test/infrastructure/database"
	"log"
)

type Container struct {
	ConfigENV          config.EnvironmentConfig
	TransactionHandler transaction.TransactionHandler
	CustomerHandler    customer.CustomerHandler
}

func SetupContainer() Container {
	fmt.Println("Loading config...")
	cfg, err := config.LoadENV()
	if err != nil {
		log.Panic(err)
	}

	fmt.Println("Loading database...")
	db, err := database.LoadPsqlDatabase(cfg.Database)
	if err != nil {
		log.Panic(err)
	}

	// INFO: initiate repository
	customerRepo := customer_repository.NewCustomerRepository(db)
	transRepo := trans_repository.NewTransactionRepository(db)

	// INFO: initiate service
	customerService := customer_service.NewCustomerService(customerRepo)
	transService := trans_service.NewTransactionService(transRepo, customerService)

	// INFO: initiate handler
	customerHandler := customer.NewCustomerHandler(customerService)
	transHandler := transaction.NewTransactionHandler(transService)

	return Container{
		ConfigENV:          cfg,
		TransactionHandler: transHandler,
		CustomerHandler:    customerHandler,
	}
}
