package delivery

import (
	"fmt"
	"net/http"

	container "isi-test/delivery/container"

	"github.com/gofiber/fiber/v2"
)

func ServerHttp(container container.Container) *fiber.App {
	app := fiber.New()

	router(app, container)
	app.Use(func(c *fiber.Ctx) error {
		resp := fiber.Map{
			"status":  fmt.Sprintf("route %s or method not allowed", http.StatusText(http.StatusNotFound)),
			"message": fmt.Sprintf("route %s", http.StatusText(http.StatusNotFound)),
		}
		return c.Status(fiber.StatusNotFound).JSON(resp)
	})

	return app
}
