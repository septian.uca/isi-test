package delivery

import (
	container "isi-test/delivery/container"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
)

func router(app *fiber.App, container container.Container) {
	app.Use(recover.New())
	app.Use(cors.New())
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowMethods: "GET,POST,PUT,PATCH,OPTION",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))

	v1 := app.Group("/v1")
	{
		transaction := v1.Group("/transaction")
		{
			transaction.Post("/tabung", container.TransactionHandler.DepositHandler)
			transaction.Post("/tarik", container.TransactionHandler.WithdrawHandler)
			transaction.Get("/saldo/:no_rekening", container.TransactionHandler.BalanceHandler)
			transaction.Get("/mutasi/:no_rekening", container.TransactionHandler.MutationListHandler)
		}

		customer := v1.Group("/customer")
		{
			customer.Post("/daftar", container.CustomerHandler.RegistatioHandler)
		}
	}
}
