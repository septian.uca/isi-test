package config

import (
	"fmt"
	infraConstant "isi-test/infrastructure/constant"
	infraConfigDB "isi-test/infrastructure/database"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

type EnvironmentConfig struct {
	App      string
	Port     int
	Env      string
	Database infraConfigDB.DatabaseConfig
}

func LoadENV() (config EnvironmentConfig, err error) {
	err = godotenv.Load()
	if err != nil {
		err = fmt.Errorf(infraConstant.ErrLoadENV, err)
		return
	}

	port, err := strconv.Atoi(os.Getenv("APP_PORT"))
	if err != nil {
		err = fmt.Errorf(infraConstant.ErrConvertStringToInt, err)
		return
	}

	config = EnvironmentConfig{
		Env:  os.Getenv("ENV"),
		App:  os.Getenv("APP_NAME"),
		Port: port,
		Database: infraConfigDB.DatabaseConfig{
			Dialect:  os.Getenv("DB_DIALECT"),
			Host:     os.Getenv("DB_HOST"),
			Name:     os.Getenv("DB_NAME"),
			Username: os.Getenv("DB_USERNAME"),
			Password: os.Getenv("DB_PASSWORD"),
		},
	}

	return
}
